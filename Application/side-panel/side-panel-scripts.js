let lastOpenedInner = false;
var previousID = "car";
document.getElementById("car").style.background = "#425c5a";

// !!! Control Side Panel Navigation menu !!!

function openNav() {

	if (document.getElementById("openSideNav").style.marginLeft === "343px") {
		if (document.getElementById("mySidenav").style.width === "335px")
			setOuterNavState("close");
		if (document.getElementById("innerSidenav").style.width === "335px")
			setInnerNavState("close");

		setPanelState("close");
	} else {
		if (lastOpenedInner === true) {
			setInnerNavState("open");
			setOuterNavState("open");

		} else {
			setOuterNavState("open");
			setInnerNavState("close");
		}

		setPanelState("open");
	}
}

function setOuterNavState(state) {

	if (state === "open") {
		lastOpenedInner = false;
		document.getElementById("mySidenav").style.width = "335px";
	}
	else {
		document.getElementById("mySidenav").style.width = "0px";
	}

}

function openInner() {
	document.getElementById("innerSidenav").style.width = "335px";
}

function closeInner() {
	document.getElementById("innerSidenav").style.width = "0px";
}

function setInnerNavState(state) {
	if (state === "open") {
		lastOpenedInner = true;
		document.getElementById("innerSidenav").style.width = "335px";
	}
	else {
		if (document.getElementById("innerSidenav").style.width === "335px")
			lastOpenedInner = true;
		else
			lastOpenedInner = false;

		document.getElementById("innerSidenav").style.width = "0px";

	}

}

function setPanelState(state) {
	if (state === "open") {
		document.getElementById("openSideNav").style.marginLeft = "343px";
		document.getElementById("openSideNav").innerHTML = "&#8678;"
	}
	else {
		document.getElementById("openSideNav").style.marginLeft = "8px";
		document.getElementById("openSideNav").innerHTML = "&#8680;"
	}
}

function swapDirectionsInput() {
	let tmp = document.getElementById("from").value;
	document.getElementById("from").value = document.getElementById("to").value;
	document.getElementById("to").value = tmp;
}

// !!! Control Travel Mode animations !!!

function setTravelModeColor(id) {
	var property = document.getElementById(id);
	if (previousID != id) {
		document.getElementById(previousID).style.backgroundColor = "#a2bfbd";
		document.getElementById(previousID).style.transition = "0.5s";
		property.style.backgroundColor = "#425c5a";
		previousID = id;
	} else
		document.getElementById(previousID).style.backgroundColor = "#425c5a";
}

function onMouseOverTravelMode(x){
	x.style.background = "#618783";
	x.style.transition = "0.5s";
	
	let travelModeImageId;
	 
	switch(x.id){
		case "car": travelModeImageId = "carImage";
			break;
		case "transit": travelModeImageId = "transitImage";
			break;
		case "walk": travelModeImageId = "walkImage";
			break;
		case "bicycle": travelModeImageId = "bicycleImage";
			break;
		default: travelModeImageId = "carImage";		
	}
	
	document.getElementById(travelModeImageId).style.transition = "0.1s ease"
	document.getElementById(travelModeImageId).style.height = "33.5px";
	document.getElementById(travelModeImageId).style.width = "33.5px";
	
	
}

function onMouseOutTravelMode(x){
	x.style.background = "#a2bfbd";
	x.style.transition = "0.5s";
	
	let travelModeImageId;
	 
	switch(x.id){
		case "car": travelModeImageId = "carImage";
			break;
		case "transit": travelModeImageId = "transitImage";
			break;
		case "walk": travelModeImageId = "walkImage";
			break;
		case "bicycle": travelModeImageId = "bicycleImage";
			break;
		default: travelModeImageId = "carImage";		
	}
	
	document.getElementById(travelModeImageId).style.transition = "0.1s ease"
	document.getElementById(travelModeImageId).style.height = "35px";
	document.getElementById(travelModeImageId).style.width = "35px";
	
	setTravelModeColor(previousID);
}
	


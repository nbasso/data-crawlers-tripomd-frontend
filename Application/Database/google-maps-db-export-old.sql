-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 07, 2020 at 05:38 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `google-maps`
--
CREATE DATABASE IF NOT EXISTS `google-maps` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `google-maps`;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
CREATE TABLE IF NOT EXISTS `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Primary Key',
  `name` varchar(50) DEFAULT NULL COMMENT 'name of the place',
  `address` varchar(128) DEFAULT NULL,
  `longtitude` float(10,6) DEFAULT NULL COMMENT 'location longtitude',
  `latitude` float(10,6) DEFAULT NULL COMMENT 'location latitude',
  `description` varchar(1024) NOT NULL COMMENT 'place description',
  `photo` varchar(4096) DEFAULT NULL COMMENT 'photo of the place using digits',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `address`, `longtitude`, `latitude`, `description`, `photo`) VALUES
(1, 'Orheiul Vechi', 'Orheiul Vechi', NULL, NULL, 'Orheiul Vechi', 'Orheiul Vechi');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

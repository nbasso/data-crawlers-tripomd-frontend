-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 10, 2020 at 10:14 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `google-maps`
--

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
CREATE TABLE IF NOT EXISTS `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Primary Key',
  `name` varchar(50) DEFAULT NULL COMMENT 'name of the place',
  `address` varchar(512) DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL COMMENT 'location latitude',
  `lng` float(10,7) DEFAULT NULL COMMENT 'location longtitude',
  `about` varchar(4096) NOT NULL COMMENT 'place description',
  `photo-dir` varchar(512) DEFAULT NULL COMMENT 'photo'' directory of the place',
  `url` varchar(512) DEFAULT NULL COMMENT 'link to the website if such exists',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `address`, `lat`, `lng`, `about`, `photo-dir`, `url`) VALUES
(1, 'Orheiul Vechi', 'Orheiul Vechi', NULL, NULL, 'Orheiul Vechi', 'Orheiul Vechi', NULL),
(2, 'Calaras', 'Calaras city', NULL, NULL, 'Best cognac in Moldova', NULL, NULL),
(3, 'Soroca fortress', '\"Strada Lev Tolstoi 52\"', 48.1614189, 28.3055573, 'Imposing 1499 4-tower fort once built from wood, now made of stone & situated by the Nistru River.', NULL, 'http://www.primsoroca.md/');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

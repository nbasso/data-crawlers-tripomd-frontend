-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 02, 2020 at 11:03 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `google-maps`
--

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
CREATE TABLE IF NOT EXISTS `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Primary Key',
  `name` varchar(50) DEFAULT NULL COMMENT 'name of the place',
  `address` varchar(512) DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL COMMENT 'location latitude',
  `lng` float(10,7) DEFAULT NULL COMMENT 'location longtitude',
  `about` varchar(4096) NOT NULL COMMENT 'place description',
  `photo-dir` varchar(512) DEFAULT NULL COMMENT 'photo'' directory of the place',
  `url` varchar(512) DEFAULT NULL COMMENT 'link to the website if such exists',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `address`, `lat`, `lng`, `about`, `photo-dir`, `url`) VALUES
(1, 'Orheiul Vechi', 'Orheiul Vechi', 47.3736954, 28.8166370, 'Orheiul Vechi', 'Orheiul Vechi', '<!DOCTYPE html><html><head> <title>Min</title> <link rel=\"stylesheet\" href=\"frontend/min/style.css\"></head><body> <div class=\"container\"> <h1>TITLE OF THE PLACE</h1> <img src=\"https://cdn1.img.sputnik.md/images/2752/91/27529180.jpg\" alt=\"poza\"/> <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.</p><small>str. XXXXXXXXXXXXXXX</small> <a href=\"frontend/first/index.html\">READ MORE</a> </div></body></html>'),
(2, 'Calaras', 'Calaras city', 45.3736954, 28.8166370, 'Best cognac in Moldova', NULL, '<!DOCTYPE html><html><head> <title>Min</title> <link rel=\"stylesheet\" href=\"frontend/min/style.css\"></head><body> <div class=\"container\"> <h1>TITLE OF THE PLACE</h1> <img src=\"https://cdn1.img.sputnik.md/images/2752/91/27529180.jpg\" alt=\"poza\"/> <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.</p><small>str. XXXXXXXXXXXXXXX</small> <a href=\"frontend/first/index.html\">READ MORE</a> </div></body></html>'),
(3, 'Soroca fortress', '\"Strada Lev Tolstoi 52\"', 48.1614189, 28.3055573, 'Imposing 1499 4-tower fort once built from wood, now made of stone & situated by the Nistru River.', NULL, 'http://www.primsoroca.md/'),
(4, 'Cricova', 'Strada Petru Ungureanu 1, Cricova 2084, Moldova', 47.1363678, 28.8571663, 'Cricova is the biggest underground wine complex, which is known worldwide for its huge labyrinths and tunnels, total length over 120 km.', NULL, 'https://cricova.md/en/'),
(5, 'Balti', 'Str. Independentei, Balti, Moldova', 47.7628212, 27.9309998, 'Pedestrian street in the center of Balti. It is the longest pedestrian street in Moldova, length is 1.3 km. ', NULL, NULL),
(6, 'Cetatea Soroca', 'Soroca, Moldova', 48.1614265, 28.3030720, 'Soroca fotress is one of those architectural structures, which will let you dive into medieval age. Fortress is built on the shore of river Nistru. ', NULL, NULL),
(7, 'Manastirea Hincu', 'Ciuculeni, Moldova', 47.0582047, 28.2918282, 'Guided tours of holy relics, artworks & ornate interiors at a nunnery founded in the 17th century.', NULL, NULL),
(8, 'Manastirea Saharna', 'Saharna, Moldova', 47.7928429, 27.7238197, 'Mountainside complex dating from the 18th century, with a colorful church & a cave monastery.', NULL, NULL),
(9, 'Chateau Purcari', 'Purcari, Stefan-Voda, Moldova', 46.5327873, 29.8331165, 'Purcari is a popular wine brand in Central and Eastern Europe. It has won over 250 medals at international wine contests.', NULL, 'https://purcari.wine/ro/'),
(10, 'Padurea Plaiul Fagului', 'Moldova', 47.2896194, 28.0604630, 'The forest Plaiul Fagului is one of the biggest and most beautiful forests in Moldova. There you can meet a Polish manor, built in XIX century.', NULL, NULL),
(11, 'Naslavcea', 'Naslavcea, Moldova', 48.4737167, 27.5646114, 'Naslavcea is a village and the northernmost point of Moldova. There you can find an unique and charming view on the river Nistru.', NULL, NULL),
(12, 'Lacul Beleu', 'Cahul district, Moldova', 45.5910492, 28.1522713, 'Lake Beleu is big and untouched natural lake. It has appeared about 5-6 thousand years ago due to tectonic movement. There you can find many species of birds and fish.', NULL, NULL),
(13, 'Windmill', 'Besalma, Moldova', 46.1689301, 28.6430931, 'One of the last working windmills in Moldova. It was built in XIX century.', NULL, NULL),
(14, 'Castel Mimi', 'Bulboaca, AN, Moldova', 46.8908615, 29.2890530, 'It is the first winemaking castle in our country. It was built in 1900 by Constantin Mimi. Underground tunnels will surprise you with their diversity and big length. ', NULL, 'www.castelmimi.md'),
(15, 'Buncarul din Soldanesti', 'Soldanesti District, Moldova', 47.7933922, 28.6745777, 'It is 13-floor underground bunker of soviet period. There are only three such bunkers in the world, the other two are in Belarus and Azerbaijan.', NULL, NULL),
(16, 'Manastirea Rudi', 'Rudi, Moldova', 48.3373489, 27.1831646, 'This monastery was built in 1772 and became a monument of architecture. ', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

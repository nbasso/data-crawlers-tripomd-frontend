import React from 'react';
import './App.css'


function mini_page(){
  return(
    <div className="container">
        <h1>TITLE OF THE PLACE</h1>
        <img src="https://cdn1.img.sputnik.md/images/2752/91/27529180.jpg" alt="poza"/>
        <p>Lorem ipsum, or lipsum as it is 
            sometimes known, is dummy text 
            used in laying out print, graphic 
            or web designs.</p>
        <small>str. XXXXXXXXXXXXXXX</small>
        <a href="App.js">READ MORE</a>
    </div>    
  );
}

export default mini_page;
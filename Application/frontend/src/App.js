import React from 'react';
import './App.css'
import 'bootstrap/dist/css/bootstrap.css';

function App(){
  return(
    <body>
      <header>
          <div>
              <div className="header-image">
              <a href="#"><img className="logo" src="https://z-p3-scontent.fkiv1-1.fna.fbcdn.net/v/t1.15752-9/126410870_675125276495442_6225597620442556302_n.png?_nc_cat=100&ccb=2&_nc_sid=ae9488&_nc_ohc=qw9gbVyiEysAX8160lj&_nc_ht=z-p3-scontent.fkiv1-1.fna&oh=de3368b3fad3b5f8953dc5e13ac5b723&oe=5FDD6301" alt="logo"/></a>
                  <h1 className="place-name">Orheiul Vechi</h1>
              </div>
          </div>
          <div className="menu-block">
              <nav className="navbar navbar-expand-lg navbar-light">
                  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                  </button>
                  <div className="collapse navbar-collapse" id="navbarNav">
                  <ul className="navbar-nav">
                      <li className="nav-item active">
                      <a className="nav-link" href="#photo-section">PHOTO<span class="sr-only">(current)</span></a>
                      </li>
                      <li className="nav-item active">
                      <a className="nav-link" href="#history-section">HISTORY</a>
                      </li>
                      <li className="nav-item active">
                      <a className="nav-link" href="#services-section">SERVICES</a>
                      </li>
                      <li className="nav-item active">
                      <a className="nav-link" href="#about-section">ABOUT</a>
                      </li>
                  </ul>
                  </div>
              </nav>
          </div>
      </header>
      <main>
        <section id="photo-section" className="photo">
          <div id="slider">
              <figure>
                  <img src="https://doxologia.ro/sites/default/files/styles/media-articol/public/articol/2017/06/00_orheiul_vechi-basarabia_foto_oana_nechifor.jpg?itok=fQgYcHWm"/>
                  <img src="https://doxologia.ro/sites/default/files/styles/media-articol/public/articol/2017/06/05_orheiul_vechi-basarabia_foto_oana_nechifor.jpg?itok=1quGJGAO"/>
                  <img src="https://4.bp.blogspot.com/-X3VkjgLxjag/U33erhlAghI/AAAAAAAAAvE/aqXGcKbj1lg/s1600/orhei_sus.jpg"/>
                  <img src="https://i.ytimg.com/vi/3omQy6NTH-w/maxresdefault.jpg"/>
                  <img src="https://doxologia.ro/sites/default/files/styles/media-articol/public/articol/2017/06/00_orheiul_vechi-basarabia_foto_oana_nechifor.jpg?itok=fQgYcHWm"/>
              </figure>
          </div>
      </section>
      <section id="history-section" className="history">
          <div className="history-content">
              <h2>History</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, ipsa. Sunt sint fugiat nisi veniam modi. Libero sit commodi nihil, reiciendis non sunt perferendis quas, repellat vitae quod at labore.
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit molestias quia mollitia explicabo ex. Officiis illo alias assumenda asperiores facilis sapiente omnis voluptatem nobis? Alias debitis quod sint exercitationem libero.
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, recusandae nisi adipisci obcaecati eum iste quas dignissimos laborum autem quia. Voluptatem odit praesentium necessitatibus, fuga nulla quod enim earum magnam.
              </p>
          </div>
      </section>
      <section id="services-section" className="services">
          <h2 className="container">Services</h2>
          <div className="container">
              <div className="row">
                  <div className="col-lg-4 col-md-6 col-sm-12">
                      <div className="services-block">
                          <img src="https://i.ytimg.com/vi/3omQy6NTH-w/maxresdefault.jpg" alt="img"/>
                          <h3>TITLE</h3>
                          <p>Lorem ipsum, or lipsum as it is 
                              sometimes known, is dummy text 
                              used in laying out print, graphic 
                              or web designs. </p>
                      </div>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-12">
                      <div className="services-block">
                          <img src="https://i.ytimg.com/vi/3omQy6NTH-w/maxresdefault.jpg" alt="img"/>
                          <h3>TITLE</h3>
                          <p>Lorem ipsum, or lipsum as it is 
                              sometimes known, is dummy text 
                              used in laying out print, graphic 
                              or web designs. </p>
                      </div>
                  </div>
                  <div className="col-lg-4 col-md-12 col-sm-12">
                      <div className="services-block">
                          <img src="https://i.ytimg.com/vi/3omQy6NTH-w/maxresdefault.jpg" alt="img"/>
                          <h3>TITLE</h3>
                          <p>Lorem ipsum, or lipsum as it is 
                              sometimes known, is dummy text 
                              used in laying out print, graphic 
                              or web designs. </p>
                      </div>
                  </div>
              </div>
          </div>
          <a href="#" className="services-button">Discover</a>
      </section>
        <footer id="about-section">
            <div className="first-footer-block">
                <div className="follow-us">
                    <h6>Follows Us</h6>
                    <a href="https://utm.md/"><img src='https://i.pinimg.com/originals/ef/b5/d9/efb5d90eeb07bb5914f094007da4c2f7.png' alt="logo"/></a>
                    <a href="https://www.facebook.com/UTMoldova"><img src='https://utm.md/wp-content/uploads/2018/12/utm-logo.png' alt="."/></a>
                    <a href="https://www.instagram.com/utmoldova/"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/1200px-Instagram_logo_2016.svg.png" alt="."/></a>
                </div>
                <div className="team">
                    <h6>Team</h6>
                    <ul>
                        <li><a href="#">Basso Nicolae</a></li>
                        <li><a href="#">Corman Daniel</a></li>
                        <li><a href="#">Mamaliga Dumitras</a></li>
                        <li><a href="#">Popescu Paula</a></li>  	
                        <li><a href="#">Tcacenco Igor</a></li> 
                    </ul>
                </div>
            </div>
            <div className="second-footer-block">
                <small>© 2020, Your company. All rights reserved.</small>
            </div>
        </footer>
      </main>
    </body>
    

    
  );
}

export default App;
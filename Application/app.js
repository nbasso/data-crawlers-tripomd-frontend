let map;

let service;
let infoWindow;
let request;

let markers = [];
let dbMarkers = [];
let travelMode;
const icon = 'marker-icon.png';

let open = false;

class Place {
	constructor(name, place) {
		this.name = name;
		this.place = place;
	}
}

function fadeOut() {

	TweenMax.to(".myBtn", 1, {
		y: -100,
		opacity: 0
	});

	TweenMax.to(".screen", 2, {
		y: -400,
		opacity: 0,
		ease: Power2.easeInOut,
		delay: 2
	});

	TweenMax.from(".overlay", 2, {
		ease: Power2.easeInOut
	});

	TweenMax.to(".overlay", 2, {
		delay: 0,
		top: "-110%",
		ease: Expo.easeInOut
	});
}

// main application method
function initMap() {
	// initialize default coordinates for when the map is loaded
	const defaultViewCoordinates = new google.maps.LatLng(46.9999648, 28.7881367);

	// initialize map by assigning it to the HTML div block classified as "map"
	map = new google.maps.Map(document.getElementById("map"), {
		center: defaultViewCoordinates,
		zoom: 8,
		mapTypeControl: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
			position: google.maps.ControlPosition.TOP_RIGHT,
		},
		styles: [
			{
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#a2bfbd"
					}
				]
			},
			{
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"elementType": "labels.text.stroke",
				"stylers": [
					{
						"color": "#454545"
					}
				]
			},
			{
				"featureType": "administrative",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#757575"
					}
				]
			},
			{
				"featureType": "administrative.country",
				"elementType": "geometry.stroke",
				"stylers": [
					{
						"color": "#545454"
					},
					{
						"weight": 1.5
					}
				]
			},
			{
				"featureType": "administrative.country",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#ffffff"
					},
					{
						"weight": 0.5
					}
				]
			},
			{
				"featureType": "administrative.land_parcel",
				"stylers": [
					{
						"lightness": 25
					},
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "administrative.locality",
				"elementType": "labels.text",
				"stylers": [
					{
						"color": "#707070"
					},
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "administrative.locality",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#383838"
					},
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "administrative.province",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#474747"
					}
				]
			},
			{
				"featureType": "administrative.province",
				"elementType": "labels.text.stroke",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#757575"
					}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#819c99"
					}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#3b3b3b"
					},
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "labels.text.stroke",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road",
				"stylers": [
					{
						"weight": 0.5
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "geometry",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "geometry.fill",
				"stylers": [
					{
						"color": "#2c2c2c"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"stylers": [
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#696969"
					},
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road.highway",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#696969"
					},
					{
						"visibility": "simplified"
					},
					{
						"weight": 0.5
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "labels",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "labels.text",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "labels.text.stroke",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road.highway.controlled_access",
				"stylers": [
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.highway.controlled_access",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#4e4e4e"
					}
				]
			},
			{
				"featureType": "road.local",
				"stylers": [
					{
						"color": "#696969"
					},
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "road.local",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#696969"
					}
				]
			},
			{
				"featureType": "road.local",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#616161"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#757575"
					}
				]
			},
			{
				"featureType": "water",
				"stylers": [
					{
						"color": "#ddebfd"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#3d3d3d"
					}
				]
			}
		]
	});

	// initialize direction's service objects
	const directionsService = new google.maps.DirectionsService();
	const directionsRenderer = new google.maps.DirectionsRenderer();
	directionsRenderer.setMap(map);

	/*document.getElementById("closeInnerNavBtn").onclick = function (){
		//directionsRenderer.setMap(null);
	}*/

	document.getElementById("closeInnerNavBtn").addEventListener("click", function () {
		directionsRenderer.setMap(null);
	});

	const onChangeHandler = function () {
		if (document.getElementById("from").value.length > 0 && document.getElementById("to").value.length > 0) {
			calculateAndDisplayRoute(directionsService, directionsRenderer);
			removeSearchMarkersFromMap();
		}
	};

	document
		.getElementById("from")
		.addEventListener("change", onChangeHandler);
	document
		.getElementById("to")
		.addEventListener("change", onChangeHandler);

	setEventHandlers();

	//
	testXML();

	const sidenav = document.getElementById("sidenav");
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(sidenav);

	// point user to his geolocation when respective button is clicked
	const locationButton = document.getElementById("pan-button");
	locationButton.classList.add("custom-map-control-button");

	locationButton.addEventListener("click", () => {
		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(
				(position) => {
					const pos = {
						lat: position.coords.latitude,
						lng: position.coords.longitude,
					};
					infoWindow.setPosition(pos);
					infoWindow.setContent("Location found.");
					infoWindow.open(map);

					// create marker on your place
					const marker = new google.maps.Marker({
						position: pos,
						map: map,

					});

					map.setCenter(pos);
				},
				() => {
					handleLocationError(true, infoWindow, map.getCenter());
				}
			);
		} else {
			// Browser doesn't support Geolocation
			handleLocationError(false, infoWindow, map.getCenter());
		}
	});

	infoWindow = new google.maps.InfoWindow();
	infoWindow.content = "infoWindow = new google.maps.InfoWindow();";
	service = new google.maps.places.PlacesService(map);

	request = {
		query: "Universitatea Tehnica a Moldovei, Rascani, Studentilor 7",
		fields: ["name", "geometry"]
	};

	//markSpecificPlace(request);
	initAutocomplete();
}

// forms request for a specific text input search and creates a market for each result
function markSpecificPlace() {
	service.findPlaceFromQuery(request, (results, status) => {
		if (status === google.maps.places.PlacesServiceStatus.OK) {
			for (let i = 0; i < results.length; i++) {
				createMarker(results[i]);
			}
			map.setCenter(results[0].geometry.location);
		}
	});
}

// prints marker(s) according to the query find results
function createMarker(place) {
	const marker = new google.maps.Marker({
		map,
		position: place.geometry.location,
		title: place.name,
		icon: icon,
	});

	const contentString =
		'<div id="content">' +
		'<div id="siteNotice">' +
		"</div>" +
		'<h1 id="firstHeading" class="firstHeading">Technical University of Moldova</h1>' +
		'<div id="bodyContent">' +
		"<p>The Technical University of Moldova was founded in 1964, under the name \"The Polytechnic Institute of Chisinau,\" as a center for engineering and economic specialties from the Moldova State University.\n" +
		"\n" +
		"The university had begun with 5140 students and 278 teachers within 5 faculties: Electrotechnics, Mechanics, Technology, Construction and Economy. Since 1964, the university has grown extensively, producing 66,000 specialists and becoming an important educational, scientific and cultural center.</p>" +
		'<p>Attribution: TUM, <a href="https://en.wikipedia.org/wiki/Technical_University_of_Moldova#:~:text=The%20Technical%20University%20of%20Moldova,be%20accredited%20by%20the%20state.">' +
		"https://en.wikipedia.org/w/index.php?title=Uluru</a> " +
		"<br> This page was last edited on 2 June 2020, at 14:18 (UTC).</p>" +
		"</div>" +
		"</div>";
	const infoWindow = new google.maps.InfoWindow({
		content: contentString,
	});

	google.maps.event.addListener(marker, "click", () => {
		infoWindow.open(map, marker);
	});
}

function createMarkerLatLng(place) {
	const marker = new google.maps.Marker({
		map,
		position: place.LatLng,
		title: place.name,

	});

	const contentString =
		'<div id="content">' +
		'<div id="siteNotice">' +
		"</div>" +
		'<h1 id="firstHeading" class="firstHeading">Technical University of Moldova</h1>' +
		'<div id="bodyContent">' +
		"<p>The Technical University of Moldova was founded in 1964, under the name \"The Polytechnic Institute of Chisinau,\" as a center for engineering and economic specialties from the Moldova State University.\n" +
		"\n" +
		"The university had begun with 5140 students and 278 teachers within 5 faculties: Electrotechnics, Mechanics, Technology, Construction and Economy. Since 1964, the university has grown extensively, producing 66,000 specialists and becoming an important educational, scientific and cultural center.</p>" +
		'<p>Attribution: TUM, <a href="https://en.wikipedia.org/wiki/Technical_University_of_Moldova#:~:text=The%20Technical%20University%20of%20Moldova,be%20accredited%20by%20the%20state.">' +
		"https://en.wikipedia.org/w/index.php?title=Uluru</a> " +
		"<br> This page was last edited on 2 June 2020, at 14:18 (UTC).</p>" +
		"</div>" +
		"</div>";
	const infoWindow = new google.maps.InfoWindow({
		content: contentString,
	});

	google.maps.event.addListener(marker, "click", () => {
		infoWindow.open(map, marker);
	});
}

/*
	handles the Search Box:
	  displays suggested locations names according to the input text (autocompletion tool);
	  if a specific place has been chosen then marks it on the map and moves the view to it;
	  otherwise displays a set of markers which are closely related to the query.
 */
function initAutocomplete() {
	// Create the search box and link it to the UI element.
	const input = document.getElementById("SearchBox");
	const searchBox = new google.maps.places.SearchBox(input);
	setSearchBoxAutoCompletion(searchBox);

	// Create starting & destination location boxes and link them to the UI elements.
	const from = document.getElementById("from");
	const to = document.getElementById("to");
	const startingSearchBox = new google.maps.places.SearchBox(from);
	const destinationSearchBox = new google.maps.places.SearchBox(to);
	setSearchBoxAutoCompletion(startingSearchBox);
	setSearchBoxAutoCompletion(destinationSearchBox);

}

// handle get user's geolocation failure
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow.setContent(
		browserHasGeolocation ?
			"Error: The Geolocation service failed." :
			"Error: Your browser doesn't support geolocation."
	);
	infoWindow.open(map);
}



//!!! test working with MySQL using PHP to retrieve data in XML format and dump in to the browser !!!
function downloadUrl(url, callback) {
	var request = window.ActiveXObject ?
		new ActiveXObject('Microsoft.XMLHTTP') :
		new XMLHttpRequest;

	request.onreadystatechange = function () {
		if (request.readyState == 4) {
			request.onreadystatechange = ({});
			callback(request, request.status);
		}
	};

	request.open('GET', url, true);
	request.send(null);
}

//
function testXML() {

	downloadUrl('http://localhost/Maps/get-data-from-db-as-xml.php', function (data) {
		let xml = data.responseXML;
		let markers = xml.documentElement.getElementsByTagName('place');

		Array.prototype.forEach.call(markers, function (markerElem) {
			//let priority = parseInt(Math.random() * 10) + 1;

			let priority = parseInt(markerElem.getAttribute('priority'));
			let id = markerElem.getAttribute('id');
			let name = markerElem.getAttribute('name');
			let address = markerElem.getAttribute('address');
			let point = new google.maps.LatLng(
				parseFloat(markerElem.getAttribute('lat')),
				parseFloat(markerElem.getAttribute('lng')));

			let content = parseXML_to_UTF(markerElem.getAttribute('url'));

			let infowincontent = document.createElement('div');
			let strong = document.createElement('strong');

			strong.textContent = name;
			infowincontent.appendChild(strong);
			infowincontent.appendChild(document.createElement('br'));

			let text = document.createElement('text');
			text.textContent = address;
			infowincontent.appendChild(text);
			//let icon = customLabel[type] || {};

			// put a marker for each place
			//createMarkerLatLng(new Place(name, point));

			let marker = new google.maps.Marker({
				map: map,
				position: point,
				priority: priority,
				icon: icon,
			});

			dbMarkers.push(marker);

			marker.addListener('click', function () {
				infoWindow.setContent(content);
				infoWindow.open(map, marker);
				open = true;
			});

			marker.addListener('mouseover', function () {
				infoWindow.setContent(content);
				infoWindow.open(map, marker);
			});

		});
	});
}

//          !!!        Directions        !!!
function calculateAndDisplayRoute(directionsService, directionsRenderer) {
	directionsService.route(
		{
			origin: {
				query: document.getElementById("from").value,
			},
			destination: {
				query: document.getElementById("to").value,
			},
			travelMode: travelMode,
		},
		(response, status) => {
			if (status === "OK") {
				directionsRenderer.setMap(map);
				directionsRenderer.setDirections(response);
			} else {
				window.alert("Directions request failed due to " + status);
			}
		}
	);
}

//          !!!        Event handlers        !!!
function setEventHandlers() {
	travelMode = google.maps.TravelMode.DRIVING;

	document.getElementById("car").addEventListener("click", function () {
		travelMode = google.maps.TravelMode.DRIVING;
	});

	document.getElementById("transit").addEventListener("click", function () {
		travelMode = google.maps.TravelMode.TRANSIT;
	});

	document.getElementById("walk").addEventListener("click", function () {
		travelMode = google.maps.TravelMode.WALKING;
	});

	document.getElementById("bicycle").addEventListener("click", function () {
		travelMode = google.maps.TravelMode.BICYCLING;
	});


	/* Change markers on zoom */
	google.maps.event.addListener(map, 'zoom_changed', function () {
		var zoom = map.getZoom();

		console.log(zoom);

		if (zoom <= 10)
			markers.forEach(marker => {
				marker.setVisible(true);
			});
		else
			markers.forEach(marker => {
				marker.setVisible(false);
			});

		dbMarkers.forEach(marker => {
			if (zoom > marker.priority)
				marker.setVisible(true);
			else
				marker.setVisible(false);
		})
	});

	/*google.maps.event.addListener(map, 'closeclick', function () {
		if(open === false) {
			infoWindow.close();
		} else {
			open = true;
		}
	});*/

	/*infoWindow.addListener(map, 'closeclick', function () {

	});*/

}

//          !!!        Search autocompletion prompter        !!!
function setSearchBoxAutoCompletion(searchBox) {
	// Bias the SearchBox results towards current map's viewport.
	map.addListener("bounds_changed", () => {
		searchBox.setBounds(map.getBounds());
	});

	/*
		Listen for the event fired when the user selects a prediction and retrieve
		more details for that place.
	*/
	searchBox.addListener("places_changed", () => {
		const places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		// Clear out the old markers.
		markers.forEach((marker) => {
			marker.setMap(null);
		});
		markers = [];

		// For each place, get the icon, name and location.
		const bounds = new google.maps.LatLngBounds();

		places.forEach((place) => {
			if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
			}

			const icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25),
			};

			// Create a marker for each place.
			markers.push(
				new google.maps.Marker({
					map,
					icon,
					title: place.name,
					photo: place.photos,
					animation: google.maps.Animation.DROP,
					position: place.geometry.location,
				})
			);

			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
		});

		// for each marker add listener to click operation, which opens popup webpage window
		markers.forEach(marker => {
			marker.addListener("click", () => {

				let imgUrl = null;
				if (marker.photo !== null && marker.photo !== undefined)
					imgUrl = marker.photo[0].getUrl();
				let img = '<img alt="no-image" width="400px" height="300px" style="margin:10px 12px 20px 12px" src="' + imgUrl + '">';
				//console.log(marker.photo[0].getUrl());
				// marker onClick() popup webpage contents to be displayed
				let contentString =
					'<div style="margin-right:12px">' +
					'<p>' + marker.title + '</p>';

				if (imgUrl !== null) {
					contentString += img;
				}

				contentString += '</div>';


				const infowindow = new google.maps.InfoWindow({
					content: contentString,
				});
				infowindow.open(map, marker);
			});

			marker.addListener("mouseover", () => {
				//if (marker.getAnimation() !== null) {
				marker.setAnimation(null);
				//} else {
				marker.setAnimation(google.maps.Animation.BOUNCE);
				//}
			});

			marker.addListener("mouseout", () => {
				marker.setAnimation(null);
			});
		});

		map.fitBounds(bounds);
	});
}

function removeSearchMarkersFromMap() { markers.forEach(marker => marker.setMap(null)) }

function parseToXML($htmlStr) {
	$xmlStr = str_replace('<', '&lt;', $htmlStr);
	$xmlStr = str_replace('>', '&gt;', $xmlStr);
	$xmlStr = str_replace('"', '&quot;', $xmlStr);
	$xmlStr = str_replace("'", '&#39;', $xmlStr);
	$xmlStr = str_replace("&", '&amp;', $xmlStr);
	return $xmlStr;
}

function parseXML_to_UTF(str) {
	let res = str
		.replace(/&lt;/g, '<')
		.replace(/&gt;/g, '>')
		.replace(/&quot;/g, '\"')
		.replace(/&apos/g, '\'');

	return res;
}